# ---------------------------------------------------------------------------------------
# Part3 Step1 task4 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/filebeat/*
echo "/var/log/filebeat/* CLEANED \r\n"

echo "------------------------------------------------------------------ /etc/filebeat/modules.d/nginx.yml ----------------------------------------------- \r\n"
sudo cat /etc/filebeat/modules.d/nginx.yml
echo "------------------------------------------------------------------ /etc/filebeat/modules.d/system.yml ----------------------------------------------- \r\n"
sudo cat /etc/filebeat/modules.d/system.yml
echo "------------------------------------------------------------------ /etc/filebeat/filebeat.yml ----------------------------------------------- \r\n"
sudo cat /etc/filebeat/filebeat.yml
echo "----------------------------------------------------------------------------------------------------------------- \r\n"

sudo service filebeat restart

echo "filebeat RESTARTED \r\n"

sleep 5
echo "5 second \r\n"
sleep 5
echo "10 second \r\n"
sleep 5
echo "15 second \r\n"
sleep 5
netstat -tulpn | grep "9000"

echo ""


