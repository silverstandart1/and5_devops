#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

cd /home/infodba

export dir_name=/home/infodba/kibana-8.1.3-amd64.deb
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
fi

if [ ! -e $dir_name ]; then

    # my link into yandex disk elasticsearch-8.1.3-amd64.deb
    /home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_yandex_disk_downloader.py https://disk.yandex.ru/d/wcHt752Sbd7qPw .
    sudo dpkg -i kibana-8.1.3-amd64.deb
fi

echo " \r\n"
sudo chmod 777 /etc/kibana/kibana.yml
sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/2_kibana.yml /etc/kibana/kibana.yml

echo " \r\n"
sudo service kibana start
sudo systemctl enable kibana

sudo rm /var/log/kibana/kibana.log
echo "/var/log/kibana/kibana.log CLEANED\к\n"

sudo service kibana restart

echo "kibana RESTARTED\r\n"

sleep 5
echo "5 second\r\n"
sleep 5
echo "10 second\r\n"
sleep 5
echo "15 second\r\n"
sleep 5
echo "20 second\r\n"
sleep 5

echo " \r\n"
sudo service kibana status
echo " \r\n"

. /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh
echo kibanaadmin:$AND5_SVR_ELKF_PASSWORD

sudo rm /etc/nginx/htpasswd.users
sudo touch /etc/nginx/htpasswd.users
sudo chmod 777 /etc/nginx/htpasswd.users

sudo echo -n 'kibanaadmin:' >> /etc/nginx/htpasswd.users
openssl passwd -apr1 $AND5_SVR_ELKF_PASSWORD >> /etc/nginx/htpasswd.users

sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/2_andrey.com.conf /etc/nginx/conf.d/andrey.com.conf
sudo nginx -t
sudo service nginx restart

echo " \r\n"
netstat -tulpn | grep "5601"
netstat -tulpn | grep "8080"

echo " \r\n"
