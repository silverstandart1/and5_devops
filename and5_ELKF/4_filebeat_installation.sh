#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

cd /home/infodba

export dir_name=/home/infodba/filebeat-8.1.3-amd64.deb
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
fi

if [ ! -e $dir_name ]; then

    # my link into yandex disk filebeat-8.1.3-amd64.deb
    /home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_yandex_disk_downloader.py https://disk.yandex.ru/d/Oq5CzkkT3mdqmw .
    sudo dpkg -i filebeat-8.1.3-amd64.deb
fi


sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/4_filebeat.yml /etc/filebeat/filebeat.yml
sudo filebeat modules enable system
sudo filebeat modules enable nginx

sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/4_nginx.yml /etc/filebeat/modules.d/nginx.yml
sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/4_system.yml /etc/filebeat/modules.d/system.yml

sudo chmod 755 /etc/filebeat/filebeat.yml
sudo chown root:root /etc/filebeat/filebeat.yml
sudo chmod 755 /etc/filebeat/modules.d/nginx.yml
sudo chown root:root /etc/filebeat/modules.d/nginx.yml
sudo chmod 755 /etc/filebeat/modules.d/system.yml
sudo chown root:root /etc/filebeat/modules.d/system.yml

sudo filebeat setup -e -E output.logstash.enabled=true -E output.elasticsearch.enabled=false -E output.logstash.host='10.5.0.7:5044' -E setup.kibana.host='10.5.0.7:5601'

sudo service filebeat restart

echo "filebeat RESTARTED\r\n"

sleep 5
echo "5 second\r\n"
sleep 5
echo "10 second\r\n"
sleep 5
echo "15 second\r\n"
sleep 5
netstat -tulpn | grep "9000"

#curl -X GET 'http://10.5.0.7:9200/filebeat-*/_search?pretty'
