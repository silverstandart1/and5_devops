#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task0 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

echo "--------------------------------------------------------------------------------------------------------------------------------Summary INFO about machines\r\n"
/home/infodba/yandex-cloud/bin/yc compute instance list

echo "--------------------------------------------- elasticsearch \r\n"
netstat -tulpn | grep "9200"
echo " \r\n"
curl -X GET "10.5.0.7:9200/_cat/health?v=true&pretty"

echo " \r\n"
echo "--------------------------------------------- kibana \r\n"
netstat -tulpn | grep "5601"
netstat -tulpn | grep "8080"

. /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh
echo kibana crenentials: kibanaadmin:$AND5_SVR_ELKF_PASSWORD 

echo " \r\n"
echo "--------------------------------------------- logstash \r\n"
netstat -tulpn | grep "5044"

echo " \r\n"
echo "--------------------------------------------- filebeat \r\n"
netstat -tulpn | grep "9000"
