# ---------------------------------------------------------------------------------------
# Part3 Step1 task3 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/logstash/logstash-plain.log
echo "/var/log/logstash/logstash-plain.log CLEANED \r\n"

echo "------------------------------------------------------------------ /etc/logstash/conf.d/andrey.conf ----------------------------------------------- \r\n"
sudo cat /etc/logstash/conf.d/andrey.conf
echo "----------------------------------------------------------------------------------------------------------------- \r\n"

sudo service logstash restart

echo "logstash RESTARTED \r\n"

sleep 5
echo "5 second \r\n"
sleep 5
echo "10 second \r\n"
sleep 5
echo "15 second \r\n"
sleep 5
echo "20 second \r\n"
sleep 5
echo "25 second \r\n"
sleep 5
echo "30 second \r\n"
sleep 5
echo "35 second \r\n"
sleep 5
echo "40 second \r\n"
sleep 5
echo "45 second \r\n"
sleep 5
netstat -tulpn | grep "5044"

echo "------------------------------------------------------------------ WARNINGS ----------------------------------------------- \r\n"
sudo cat /var/log/logstash/logstash-plain.log | grep "WARN"
echo "------------------------------------------------------------------ ERRORS ----------------------------------------------- \r\n"
sudo cat /var/log/logstash/logstash-plain.log | grep "ERROR"

echo ""

