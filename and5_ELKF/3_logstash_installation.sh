#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------
#----------------------------------------------------------------------- Installation logstash

cd /home/infodba

export dir_name=/home/infodba/logstash-8.1.3-amd64.deb
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
fi

if [ ! -e $dir_name ]; then

    # my link into yandex disk logstash-8.1.3-amd64.deb
    /home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_yandex_disk_downloader.py https://disk.yandex.ru/d/Ea5j0NHvkFCbrw .
    sudo dpkg -i logstash-8.1.3-amd64.deb
fi

sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/3_andrey.conf /etc/logstash/conf.d/andrey.conf

#validation
#sudo -u logstash /usr/share/logstash/bin/logstash --path.settings /etc/logstash -t
#could be run as debug
#sudo /usr/share/logstash/bin/logstash -f /etc/logstash/conf.d/andrey.conf --path.settings /etc/logstash --verbose

sudo service logstash start
sudo systemctl enable logstash

if [ -f /var/log/logstash/logstash-plain.log ]; then
    sudo rm /var/log/logstash/logstash-plain.log
    echo "/var/log/logstash/logstash-plain.log CLEANED\r\n"
fi

sudo service logstash restart

echo "logstash RESTARTED\r\n"

sleep 5
echo "5 second\r\n"
sleep 5
echo "10 second\r\n"
sleep 5
echo "15 second\r\n"
sleep 5
echo "20 second\r\n"
sleep 5
echo "25 second\r\n"
sleep 5
echo "30 second\r\n"
sleep 5
echo "35 second\r\n"
sleep 5
echo "40 second\r\n"
sleep 5
echo "45 second\r\n"
sleep 5

sudo service logstash status

netstat -tulpn | grep "5044"


