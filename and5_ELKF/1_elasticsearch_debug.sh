# ---------------------------------------------------------------------------------------
# Part3 Step1 task1 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/elasticsearch/elasticsearch_deprecation.json
echo "/var/log/elasticsearch/elasticsearch_deprecation.json CLEANED\r\n"

sudo rm /var/log/elasticsearch/elasticsearch.log
echo "/var/log/elasticsearch/elasticsearch.log CLEANED\r\n"

sudo rm /var/log/elasticsearch/elasticsearch_server.json
echo "/var/log/elasticsearch/elasticsearch_server.json CLEANED\r\n"

echo "------------------------------------------------------------------ /etc/elasticsearch/jvm.options -----------------------------------------------\r\n"
sudo cat /etc/elasticsearch/jvm.options
echo "------------------------------------------------------------------ /etc/elasticsearch/elasticsearch.yml -----------------------------------------------\r\n"
sudo cat /etc/elasticsearch/elasticsearch.yml
echo "-----------------------------------------------------------------------------------------------------------------\r\n"

sudo service elasticsearch restart
echo "elasticsearch RESTARTED\r\n"

sudo systemctl daemon-reload
sleep 5
echo "5 second\r\n"
sleep 5
netstat -tulpn | grep "9200"
echo " \r\n"
curl -X GET "10.5.0.7:9200/_cat/health?v=true&pretty"

echo "------------------------------------------------------------------ WARNINGS -----------------------------------------------\r\n"
sudo cat /var/log/elasticsearch/elasticsearch.log | grep "WARN"
echo "------------------------------------------------------------------ ERRORS -----------------------------------------------\r\n"
sudo cat /var/log/elasticsearch/elasticsearch.log | grep "ERROR"

echo ""
