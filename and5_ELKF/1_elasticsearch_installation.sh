#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task1 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

cd /home/infodba

export dir_name=/home/infodba/elasticsearch-8.1.3-amd64.deb
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
fi

if [ ! -e $dir_name ]; then

    # my link into yandex disk elasticsearch-8.1.3-amd64.deb
    /home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_yandex_disk_downloader.py https://disk.yandex.ru/d/vEgJIKT7N-rD_A .
    sudo dpkg -i elasticsearch-8.1.3-amd64.deb
fi

echo " \r\n"
echo " Replacement 1 value in 1_elasticsearch.yml\r\n"
. /home/infodba/0_SSH/AND5_SVR_ID.sh
sed -i "s/@AND5_SVR_ID@/$AND5_SVR_ID/" /home/infodba/AND5_DEV_HOME/11/and5_ELKF/1_elasticsearch.yml

sudo chmod 777 /etc/elasticsearch
sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/1_elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
sudo cp /home/infodba/AND5_DEV_HOME/11/and5_ELKF/1_jvm.options /etc/elasticsearch/jvm.options

sudo systemctl daemon-reload
sudo service elasticsearch start

sudo rm /var/log/elasticsearch/elasticsearch_deprecation.json
echo "/var/log/elasticsearch/elasticsearch_deprecation.json CLEANED\r\n"
sudo rm /var/log/elasticsearch/elasticsearch.log
echo "/var/log/elasticsearch/elasticsearch.log CLEANED\r\n"
sudo rm /var/log/elasticsearch/elasticsearch_server.json
echo "/var/log/elasticsearch/elasticsearch_server.json CLEANED\r\n"

sudo service elasticsearch restart

sleep 5
echo "5 second\r\n"
sleep 5
netstat -tulpn | grep "9200"

echo " \r\n"
curl -X GET "10.5.0.7:9200/_cat/health?v=true&pretty"

echo " \r\n"

# ----- elasticsearch PROPERTIES -----
#https://sleeplessbeastie.eu/2020/07/06/how-to-display-default-elasticsearch-settings/
#curl --silent "http://10.5.0.7:9200/_cluster/settings?pretty=true"
#curl --silent "http://10.5.0.7:9200/_cluster/settings?include_defaults=true&pretty=true"
