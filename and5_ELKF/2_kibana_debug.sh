# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/kibana/kibana.log
echo "/var/log/kibana/kibana.log CLEANED \r\n"

echo "------------------------------------------------------------------ /etc/kibana/kibana.yml ----------------------------------------------- \r\n"
sudo cat /etc/kibana/kibana.yml
echo "------------------------------------------------------------------ /etc/nginx/conf.d/andrey.com.conf ----------------------------------------------- \r\n"
sudo cat /etc/nginx/conf.d/andrey.com.conf
echo "----------------------------------------------------------------------------------------------------------------- \r\n"

sudo service kibana restart

echo "kibana RESTARTED \r\n"

sleep 5
echo "5 second \r\n"
sleep 5
echo "10 second \r\n"
sleep 5
echo "15 second \r\n"
sleep 5
netstat -tulpn | grep "5601"
netstat -tulpn | grep "8080"

echo "------------------------------------------------------------------ WARNINGS ----------------------------------------------- \r\n"
sudo cat /var/log/kibana/kibana.log | grep "WARN"
echo "------------------------------------------------------------------ ERRORS ----------------------------------------------- \r\n"
sudo cat /var/log/kibana/kibana.log | grep "ERROR"

echo ""
