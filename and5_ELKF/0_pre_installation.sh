#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task0 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Installation JAVA on and5-srv\r\n"
sudo apt-get update
sudo apt-get install openjdk-11-jdk wget apt-transport-https curl gpgv gpgsm gnupg-l10n gnupg dirmngr -y
echo " \r\n"
java -version

echo " \r\n"
echo "-------------------------------------------------------------------------------- Installation NGINX on and5-srv\r\n"
sudo apt-get install nginx -y
sudo service nginx start
sudo systemctl enable nginx
sleep 3
echo " \r\n"
sudo service nginx status
