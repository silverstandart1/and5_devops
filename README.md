# and5_devops



## Description

This gitlab project contains final project of DEVOPS course. 
As a cloud it is uses Yandex-Cloud
main application is devided into 2 PODs: 
- Django database (image: postgres:13.2)
- Python web application (custom [image](https://gitlab.com/silverstandart1/and5_devops/container_registry/3164029) which is building from this [Dockerfile](https://gitlab.com/silverstandart1/and5_devops/-/blob/main/and5_app/Dockerfile))

LOGs collection includes connected:
- Elasticsearch
- Kibana
- Logstash
- Filebeat and python app is sending LOGs directly into Filebeat

Monitoring system includes connected:
- Prometheus
- Node-Exporter
- Blackbox-Exporter
- Grafana


## Installation [more detailed instruction](https://gitlab.com/silverstandart1/and5_devops/-/blob/main/and5_deployment_instruction_FINAL.pdf)
### Step 0 Pre-Start actions

1. SSH keys needs to be generated 

```
ssh-keygen -t rsa -b 4096 -C infodba -f ./infodba
```
as a result you will have 2 keys (private infodba  and public infodba.pub)

2. Copy and paste content of public ssh key <content_of_public_ssh_key> into  [cloud_config.yaml](https://gitlab.com/silverstandart1/and5_devops/-/blob/main/and5_terraform/cloud_config.yaml)

3. Modify values in [terraform main.tf](https://gitlab.com/silverstandart1/and5_devops/-/blob/main/and5_terraform/main.tf):

- yandex_access_token
- yandex_cloud_id
- yandex_folder_id
- 3 paths (ssh-keys and user-data)

### Step 1 full cloud architecture deployment

4. Run this command in and5_terraform folder

```
terraform apply -auto-approve
```

### Step 2 full software structure deployment

5. Create or change variables in gitlab if they are changed after step 1:

* AND5_ACCESS_TOKEN - yandex cloud access token
* AND5_APP_SERVER_EXTERNAL_IP - external IP address after STEP 1
* AND5_INFODBA_SSH_PRIVATE_KEY - copy paste private key from STEP 0
* AND5_SVR_ELKF_PASSWORD - password for kibana
* AND5_SVR_ID - and5-svr ID after STEP 1
* AND5_SVR_PUBLIC_IP - external IP address after STEP 1

Start CICD JOBs
After CICD execution we have detailed info about structure of cluster and IP addresses of components

### Step 3 Grafana adjustment
For the first login grafana credentials admin/admin

6. Add data source for grafana

7. Import dashboard from file [and5_grafana.json](https://gitlab.com/silverstandart1/and5_devops/-/blob/main/and5_PNBG/and5_grafana.json)

## Authors and acknowledgment
Author is Andrey that is why prefix is and5_ 

## License
Anyone could use for any purposes.
