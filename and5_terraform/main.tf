# -------------------------------------------------------- VARIABLES
variable "zone" {                                # Используем переменную для передачи в конфиг инфраструктуры
  description = "Use specific availability zone" # Опционально описание переменной
  type        = string                           # Опционально тип переменной
  default     = "ru-central1-a"                  # Опционально значение по умолчанию для переменной
}
variable "token" {                            
  type        = string                           # Опционально тип переменной
  default     = "<yandex_access_token>"           # Опционально значение по умолчанию для переменной
}
variable "cloud_id" {                            
  type        = string                           # Опционально тип переменной
  default     = "<yandex_cloud_id>"           # Опционально значение по умолчанию для переменной
}
variable "folder_id" {                            
  type        = string                           # Опционально тип переменной
  default     = "<yandex_folder_id>"           # Опционально значение по умолчанию для переменной
}
variable "sa_id" {                            
  type        = string                           # Опционально тип переменной
  default     = "serviceAccount:<and5_serviceAccountID>"           # Опционально значение по умолчанию для переменной
}

# -------------------------------------------------------- PROVIDER
terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.74.0"
    }
  }
}
provider "yandex" {
  token     = var.token			   
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

# -------------------------------------------------------- WORKING CODE
# --------------------------------- Networks
resource "yandex_vpc_network" "and5-net" { 
  name = "and5-net" 
}
resource "yandex_vpc_subnet" "and5-subnet" {
  name = "and5-subnet"
  v4_cidr_blocks = ["10.5.0.0/16"]
  zone           = var.zone
  network_id     = yandex_vpc_network.and5-net.id
}

# --------------------------------- Service Account
resource "yandex_iam_service_account" "sa" {
  name        = "sa"
  description = "service account to manage with VMs"
}
resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id   = var.folder_id
  role        = "editor"
  members     = [ replace(var.sa_id, "<and5_serviceAccountID>", resource.yandex_iam_service_account.sa.id), ]
  depends_on = [ yandex_iam_service_account.sa, ]
}
resource "yandex_resourcemanager_folder_iam_binding" "puller" {
  folder_id   = var.folder_id
  role        = "container-registry.images.puller"
  members     = [ replace(var.sa_id, "<and5_serviceAccountID>", resource.yandex_iam_service_account.sa.id) ]
}

# --------------------------------- Node Group
resource "yandex_kubernetes_node_group" "and5-masters" {
  cluster_id = yandex_kubernetes_cluster.and5-cluster.id
  name = "and5-masters"  
  
  depends_on = [
    yandex_iam_service_account.sa,
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_vpc_network.and5-net,
    yandex_vpc_subnet.and5-subnet,
  ]

  instance_template {
    resources {
      cores = 2
      memory = 4
      core_fraction = 20
    }

    boot_disk {
      type = "network-hdd"
      size = 96
    }

    network_interface {
      subnet_ids = [ yandex_vpc_subnet.and5-subnet.id, ]
      nat = true
    }
	
    metadata = {
      ssh-keys = "infodba:${file("/home/andrey/DEV_HOME/11/infodba.pub")}"
    }
  }
  
  scale_policy {
    fixed_scale {
      size = 1
    }
  }
  allocation_policy {}  
  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }  
}

resource "yandex_kubernetes_node_group" "and5-workers" {
  cluster_id = yandex_kubernetes_cluster.and5-cluster.id
  name = "and5-workers"
  
  depends_on = [
    yandex_iam_service_account.sa,
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_vpc_network.and5-net,
    yandex_vpc_subnet.and5-subnet,
  ]

  instance_template {
    resources {
      cores = 2
      memory = 4
      core_fraction = 20
    }

    boot_disk {
      type = "network-hdd"
      size = 96
    }

    network_interface {
      subnet_ids = [ yandex_vpc_subnet.and5-subnet.id, ]
      nat = true
    }
	
    metadata = {
      ssh-keys = "infodba:${file("/home/andrey/DEV_HOME/11/infodba.pub")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }
  allocation_policy {}
  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }  
}

# --------------------------------- Cluster
resource "yandex_kubernetes_cluster" "and5-cluster" {
 name        = "and5-cluster"
 cluster_ipv4_range = "10.1.0.0/16"
 service_ipv4_range = "10.2.0.0/16"
 network_id = yandex_vpc_network.and5-net.id
 master {
   public_ip = true
   zonal {
     zone      = yandex_vpc_subnet.and5-subnet.zone
     subnet_id = yandex_vpc_subnet.and5-subnet.id
   }
 }
 service_account_id      = resource.yandex_iam_service_account.sa.id
 node_service_account_id = resource.yandex_iam_service_account.sa.id
   depends_on = [     
     yandex_iam_service_account.sa,
	 yandex_resourcemanager_folder_iam_binding.puller,
     yandex_resourcemanager_folder_iam_binding.editor,
   ]
}


# -------------------------------------------------------- SVR Server
data "yandex_compute_image" "ubuntu-2004" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "and5-svr" {
  name               = "and5-svr"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu-2004.id
      size = 30
      type = "network-hdd"
    }
  }


  network_interface {
    subnet_id = yandex_vpc_subnet.and5-subnet.id
    ip_address = "10.5.0.7"
    nat       = true
  }

  metadata = {
    user-data = file("/home/andrey/DEV_HOME/11/cloud_config.yaml")
  }
}

# -------------------------------------------------------- OUTPUT
output "and5-masters_status" {
  value = "${yandex_kubernetes_node_group.and5-masters.status}"
}
output "and5-workers_status" {
  value = "${yandex_kubernetes_node_group.and5-workers.status}"
}
output "and5-srv_status" {
  value = "${yandex_compute_instance.and5-svr.status}"
}

output "cluster_external_v4_endpoint" {
  value = "${yandex_kubernetes_cluster.and5-cluster.master[0].external_v4_endpoint}"
}
output "cluster_internal_v4_endpoint" {
  value = "${yandex_kubernetes_cluster.and5-cluster.master[0].internal_v4_endpoint}"
}
output "internal_ip_address_svr" {
  value = yandex_compute_instance.and5-svr.network_interface.0.ip_address
}
output "external_ip_address_svr" {
  value = yandex_compute_instance.and5-svr.network_interface.0.nat_ip_address
}
