#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script for Prometheus Node-exporter Blackbox-exporter Grafana installation on and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Create SSH connection template into and5-svr\r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr

echo " \r\n"
echo "-------------------------------------------------------------------------------- Docker installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_docker_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Prometheus Node-exporter Blackbox-exporter Grafana installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_PNBG/and5_run_PNBG.sh;"

echo " \r\n"

