#!/bin/sh
# ---------------------------------------------------------------------------------------
# Pre script for Custer deployment and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Create SSH connection template into and5-svr\r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr

echo " \r\n"
echo "-------------------------------------------------------------------------------- GIT installation and pull into and5-svr\r\n"
$and5_svr "sudo apt-get update; sudo apt-get install git -y;"

$and5_svr "/home/infodba/and5_clean_folder.sh /home/infodba/AND5_DEV_HOME; /home/infodba/and5_clean_folder.sh /home/infodba/AND5_DEV_HOME/11; git clone https://gitlab.com/silverstandart1/and5_devops.git /home/infodba/AND5_DEV_HOME/11; chmod 777 /home/infodba/AND5_DEV_HOME/11/and5_scripts/*; chmod 777 /home/infodba/AND5_DEV_HOME/11/and5_ELKF/*; chmod 777 /home/infodba/AND5_DEV_HOME/11/and5_PNBG/*;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Network tools installation into and5-svr\r\n"
$and5_svr "sudo apt-get install net-tools -y;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Yandex console installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_yandex_console_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Helm installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_helm_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Kubectl installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_kubectl_installation.sh;"

echo " \r\n"

