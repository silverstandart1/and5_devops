#!/bin/sh
# ---------------------------------------------------------------------------------------
# Prints full info about all parts / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Create SSH connection template into and5-svr\r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr

echo " \r\n"
echo "-------------------------------------------------------------------------------- Cluster full INFO\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_0_full_cluster_info.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- ELKF full INFO in and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/0_full_info_ELKF.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- PNBG full INFO in and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_PNBG/0_full_info_PNBG.sh;"
