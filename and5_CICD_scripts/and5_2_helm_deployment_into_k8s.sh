#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script for Custer deployment and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Create SSH connection template into and5-svr\r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr

echo " \r\n"
echo "-------------------------------------------------------------------------------- and5-srv Runner Yandex Console Configuration\r\n"
echo " \r\n"
$and5_svr "/home/infodba/yandex-cloud/bin/yc managed-kubernetes cluster get-credentials and5-cluster --external --force;"

echo " \r\n"
$and5_svr "kubectl config view"

echo " \r\n"
echo "-------------------------------------------------------------------------------- and5_helm_chart Deployment\r\n"
$and5_svr "cd /home/infodba/AND5_DEV_HOME/11/and5_helm_chart; helm --namespace and5-namespace upgrade --create-namespace --install and5app .;"

echo "-------------------------------------------------------------------------------- Waiting that PODs are up and running\r\n"
echo " waiting passed seconds - 10(60)\r\n"
sleep 10
echo " waiting passed seconds - 20(60)\r\n"
sleep 10
echo " waiting passed seconds - 30(60)\r\n"
sleep 10
echo " waiting passed seconds - 40(60)\r\n"
sleep 10
echo " waiting passed seconds - 50(60)\r\n"
sleep 10
echo " waiting passed seconds - 60(60)\r\n"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Cluster INFO\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_0_full_cluster_info.sh;"

echo " \r\n"
