#!/bin/sh
# ---------------------------------------------------------------------------------------
# Pre script for Custer deployment / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Update SSH Agent\r\n"
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)

echo " \r\n"
echo "-------------------------------------------------------------------------------- Copy Keys\r\n"

mkdir -p /root/DEV_HOME
chmod 777 /root/DEV_HOME
mkdir -p /root/DEV_HOME/0_SSH
chmod 777 /root/DEV_HOME/0_SSH

cp $AND5_INFODBA_SSH_PRIVATE_KEY /root/DEV_HOME/0_SSH/infodba
chmod 700 /root/DEV_HOME/0_SSH/infodba

echo " \r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr
$and5_svr -o "StrictHostKeyChecking no" "ls -l;"
echo " \r\n"

echo 'put /builds/silverstandart1/and5_devops/and5_scripts/and5_clean_folder.sh' | sftp -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP":/home/infodba
echo " \r\n"

$and5_svr "chmod 777 /home/infodba/and5_clean_folder.sh; /home/infodba/and5_clean_folder.sh /home/infodba/0_SSH;"
echo " \r\n"

echo 'put /root/DEV_HOME/0_SSH/infodba'                                            | sftp -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP":/home/infodba/0_SSH
echo " \r\n"

$and5_svr "touch /home/infodba/0_SSH/AND5_ACCESS_TOKEN.sh; chmod 777 /home/infodba/0_SSH/AND5_ACCESS_TOKEN.sh; echo 'export AND5_ACCESS_TOKEN='$AND5_ACCESS_TOKEN >> /home/infodba/0_SSH/AND5_ACCESS_TOKEN.sh; cat /home/infodba/0_SSH/AND5_ACCESS_TOKEN.sh;"

$and5_svr "touch /home/infodba/0_SSH/AND5_SVR_ID.sh; chmod 777 /home/infodba/0_SSH/AND5_SVR_ID.sh; echo 'export AND5_SVR_ID='$AND5_SVR_ID >> /home/infodba/0_SSH/AND5_SVR_ID.sh; cat /home/infodba/0_SSH/AND5_SVR_ID.sh;"

$and5_svr "touch /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh; chmod 777 /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh; echo 'export AND5_SVR_ELKF_PASSWORD='$AND5_SVR_ELKF_PASSWORD >> /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh; cat /home/infodba/0_SSH/AND5_SVR_ELKF_PASSWORD.sh;"

$and5_svr "touch /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh; chmod 777 /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh; echo 'export AND5_APP_SERVER_EXTERNAL_IP='$AND5_APP_SERVER_EXTERNAL_IP >> /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh; cat /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh;"


echo " \r\n"
