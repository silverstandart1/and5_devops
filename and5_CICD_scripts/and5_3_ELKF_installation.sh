#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script for ElasticSearch Kabana Logstash Filebeat installation on and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- Create SSH connection template into and5-svr\r\n"
export and5_svr="ssh -i /root/DEV_HOME/0_SSH/infodba infodba@"$AND5_SVR_PUBLIC_IP" "
echo $and5_svr

echo " \r\n"
echo "-------------------------------------------------------------------------------- Java, NGINX installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/0_pre_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Elasticsearch installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/1_elasticsearch_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Kibana installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/2_kibana_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Logstash installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/3_logstash_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Filebeat installation into and5-svr\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/4_filebeat_installation.sh;"

echo " \r\n"
echo "-------------------------------------------------------------------------------- Restart above services and LOG cleaning\r\n"
$and5_svr "/home/infodba/AND5_DEV_HOME/11/and5_ELKF/0_0____STARTS_ALL.sh;"

echo " \r\n"
