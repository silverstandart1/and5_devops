helm list --all-namespaces
echo -e "--------------------------------------------------------------------------------------------------------------------------------DISK and PV\n"
yc compute disk list
kubectl get pv

echo -e "--------------------------------------------------------------------------------------------------------------------------------NODEs and PODs\n"
yc managed-kubernetes node-group list
yc compute instance list
kubectl get nodes -o wide
echo -e "\n"
kubectl get pods --all-namespaces
echo -e "\n"
kubectl get services -o wide --all-namespaces
echo -e "\n"
kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces
echo -e "--------------------------------------------------------------------------------------------------------------------------------Precisely for me\n"
kubectl --namespace and5-namespace get pods -l project=and5-project -o wide
echo -e "\n"
kubectl get pods -l project=and5-project -o yaml | grep podIP
echo -e "\n"
kubectl --namespace and5-namespace get pods -l project=and5-project -o YAML | grep podIP
echo -e "\n"
