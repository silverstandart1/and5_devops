#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script supposes to be executed in cloud in and5-srv / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- and5-srv Runner Yandex Console Installation and configuration\n"
curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
export PATH=/home/infodba/.local/bin:/home/infodba/yandex-cloud/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
echo -e "AND5_ACCESS_TOKEN=$AND5_ACCESS_TOKEN"
echo -e "===1\n"
/home/infodba/yandex-cloud/bin/yc config set token $AND5_ACCESS_TOKEN
echo -e "===2\n"
echo -e "\n"
yc managed-kubernetes cluster get-credentials and5-cluster --external --force
echo -e "\n"
kubectl config view

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- and5_helm_chart Deployment\n"
cd /home/infodba/DEVHOME/11/and5_helm_chart
helm --namespace and5-namespace upgrade --create-namespace --install and5app .

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Cluster INFO\n"
chmod 777 /home/infodba/DEVHOME/11/and5_scripts/and5_0_info.sh
/home/infodba/DEVHOME/11/and5_scripts/and5_0_info.sh
