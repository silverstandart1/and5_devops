#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script supposes to be executed in cloud in and5-srv / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo " \r\n"
echo "-------------------------------------------------------------------------------- and5-srv Runner Yandex Console Configuration\r\n"
echo " \r\n"
yc managed-kubernetes cluster get-credentials and5-cluster --external --force
echo " \r\n"
kubectl config view

echo " \r\n"
echo "-------------------------------------------------------------------------------- and5_helm_chart Deployment\r\n"
cd /home/infodba/AND5_DEV_HOME/11/and5_helm_chart
helm --namespace and5-namespace upgrade --create-namespace --install and5app .

echo " \r\n"
echo "-------------------------------------------------------------------------------- Cluster INFO\r\n"
chmod 777 /home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_0_info.sh
/home/infodba/AND5_DEV_HOME/11/and5_scripts/and5_0_info.sh
