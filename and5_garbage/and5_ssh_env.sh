#!/bin/sh
# ---------------------------------------------------------------------------------------
# Pre script for Custer deployment / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Update SSH Agent\n"
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Copy Keys\n"
mkdir -p ~/DEV_HOME/0_SSH
chmod 700 ~/DEV_HOME/0_SSH
cp $AND5_INFODBA_SSH_PRIVATE_KEY ~/DEV_HOME/0_SSH/infodba
chmod 700 ~/DEV_HOME/0_SSH/infodba

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Yandex Console installation\n"
curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
/root/yandex-cloud/bin/yc --help
/root/yandex-cloud/bin/yc config set token $AND5_ACCESS_TOKEN
cd /root/yandex-cloud/bin
ls -l

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Kubectl installation\n"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

echo -e "\n"
echo -e "-------------------------------------------------------------------------------- Helm installation\n"
chmod 777 ~/DEV_HOME/
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 777 get_helm.sh
./get_helm.sh
