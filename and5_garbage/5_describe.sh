echo -e "-------------------------------------------------------------------------------------------------------------------------------- DB\n"
kubectl --namespace and5-namespace describe pods and5-db
echo -e "-------------------------------------------------------------------------------------------------------------------------------- APP\n"
kubectl --namespace and5-namespace describe pods and5-app
echo -e "-------------------------------------------------------------------------------------------------------------------------------- DB Connection\n"
kubectl --namespace and5-namespace describe svc and5-db
echo -e "-------------------------------------------------------------------------------------------------------------------------------- External Connection\n"
kubectl --namespace and5-namespace describe svc and5-app
