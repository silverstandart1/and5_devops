#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from time import sleep
import socket
import logging
from threading import Thread
from datetime import datetime

def and5_send_to_filebeat(p_message):
    try:
        now = datetime.now()
        and5_timestamp = now.strftime("%d/%m/%Y %H:%M:%S")
        and5_filebeat_host = "10.5.0.7"
        and5_filebeat_port = 9000
        and5_out_message = and5_timestamp+ " "+p_message
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((and5_filebeat_host, and5_filebeat_port))
            sock.send(and5_out_message.encode()) 
    except:
        print ('error/exception')
            

def and5_task(sleep_time, message): 
    i = 1
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    and5_ip=s.getsockname()[0]
    
    Log_Format = "%(asctime)s %(levelname)s - %(message)s"
    logging.basicConfig(filename = "/var/log/and5_log.txt",
                    filemode = "w",
                    format = Log_Format, 
                    level = logging.INFO)
    logger = logging.getLogger()

    and5_main_message = ' on ' + socket.gethostname() + "  with IP: "+and5_ip + " \r\n"
    logging.info(message+ and5_main_message)
    and5_send_to_filebeat(message+ and5_main_message)
    
    while(True):        
        logging.info("Running "+str(i)+" (minutes) "+ and5_main_message) 
        and5_send_to_filebeat("Running "+str(i)+" (minutes) "+ and5_main_message)                 
        i = i + 1
        sleep(60)    


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')
    
    and5_thread = Thread(target=and5_task, args=(1.5, 'Started and5-app LOG'))
    and5_thread.setDaemon(True) 
    and5_thread.start()

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc     

    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
