#!/bin/sh
# ---------------------------------------------------------------------------------------
#   Test Prometeus Image with Docker / 2022_04_08 / ANa
# ---------------------------------------------------------------------------------------

echo ------------------------------------------------------ Clear OLD metrics
docker rm -v -f $(docker ps -qa)



echo " \r\n"
echo ------------------------------------------------------ Create temp folder prometheus
sudo rm -rf /home/infodba/prometeus_root

mkdir /home/infodba/prometeus_root
chmod 777 /home/infodba/prometeus_root
cd /home/infodba/prometeus_root

mkdir ./prometheus
chmod 777 ./prometheus
mkdir ./blackbox
chmod 777 ./blackbox
mkdir ./grafana
chmod 777 ./grafana
mkdir ./grafana/provisioning
chmod 777 ./grafana/provisioning



echo " \r\n"
echo ------------------------------------------------------ Create prometheus/prometheus.yml
cat << EOF > ./prometheus/prometheus.yml
# ----------------------------- prometheus/prometheus.yml START --------------------
scrape_configs:
  - job_name: node
    scrape_interval: 5s
    static_configs:
    - targets: ['node-exporter:9100']

  - job_name: blackbox
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
      - targets:
        - http://@AND5_APP_SERVER_EXTERNAL_IP@:30777
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: blackbox-exporter:9115
# ----------------------------- prometheus/prometheus.yml END  --------------------
EOF

echo ------------------------------------------------------ define monitoring target
. /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh
sed -i "s/@AND5_APP_SERVER_EXTERNAL_IP@/$AND5_APP_SERVER_EXTERNAL_IP/" /home/infodba/prometeus_root/prometheus/prometheus.yml
cat /home/infodba/prometeus_root/prometheus/prometheus.yml


echo " \r\n"
echo ------------------------------------------------------ Create blackbox/blackbox.yml
cat << EOF > ./blackbox/blackbox.yml
# ----------------------------- blackbox/blackbox.yml START --------------------
modules:
  http_2xx:
    http:
      no_follow_redirects: false
      preferred_ip_protocol: ip4
      valid_http_versions:
      - HTTP/1.1
      - HTTP/2
      valid_status_codes: []
    prober: http
    timeout: 10s
# ----------------------------- blackbox/blackbox.yml END  --------------------
EOF
cat ./blackbox/blackbox.yml



echo " \r\n"
echo ------------------------------------------------------ Create docker-compose.yml
cat << EOF > ./docker-compose.yml
# ----------------------------- docker-compose.yml START --------------------
version: '3.2'
services:
    prometheus:
        image: prom/prometheus:latest
        volumes:
            - ./prometheus:/etc/prometheus/
        hostname: prometheus
        command:
            - --config.file=/etc/prometheus/prometheus.yml
        ports:
            - 10.5.0.7:9090:9090
        restart: always

    node-exporter:
        image: prom/node-exporter
        volumes:
            - /proc:/host/proc:ro
            - /sys:/host/sys:ro
            - /:/rootfs:ro
        hostname: node-exporter
        command:
            - --path.procfs=/host/proc
            - --path.sysfs=/host/sys
            - --collector.filesystem.ignored-mount-points
            - ^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)
        ports:
            - 127.0.0.1:9100:9100
        restart: always

    blackbox-exporter:
        image: prom/blackbox-exporter
        volumes:
            - ./blackbox:/config
        hostname: blackbox-exporter
        command: 
            - --config.file=/config/blackbox.yml
        ports:
            - 127.0.0.1:9115:9115
        restart: always

    grafana:
        image: grafana/grafana
        volumes:
            - ./grafana:/var/lib/grafana
            - ./grafana/provisioning/:/etc/grafana/provisioning/
        depends_on:
            - prometheus
        ports:
            - 10.5.0.7:3000:3000
        restart: always
        
# ----------------------------- docker-compose.yml END  --------------------
EOF
cat ./docker-compose.yml




echo " \r\n"
echo ------------------------------------------------------ Create Image and Run
sudo chmod 777 /var/run/docker.sock
sudo chmod 777 /var/lib
sudo docker-compose build --no-cache
sudo docker-compose up -d
sleep 5
echo "\n"
docker ps
echo "\ngrafana available in  127.0.0.1:3000 with credentials admin / admin\n"
