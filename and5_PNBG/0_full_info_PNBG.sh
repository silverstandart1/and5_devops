#!/bin/sh
# ---------------------------------------------------------------------------------------
# Part3 Step1 task0 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

echo "--------------------------------------------- overal about PNBG \r\n"
sudo chmod 777 /var/run/docker.sock
docker ps

echo "--------------------------------------------- prometheus \r\n"
. /home/infodba/0_SSH/AND5_APP_SERVER_EXTERNAL_IP.sh
echo "watching target for prometeus is http://"$AND5_APP_SERVER_EXTERNAL_IP:30777
netstat -tulpn | grep "9090"

echo " \r\n"
echo "--------------------------------------------- node-exporter \r\n"
netstat -tulpn | grep "9100"

echo " \r\n"
echo "--------------------------------------------- blackbox-exporter \r\n"
netstat -tulpn | grep "9115"

echo " \r\n"
echo "--------------------------------------------- grafana \r\n"
echo grafana crenentials: admin:admin
netstat -tulpn | grep "3000"
