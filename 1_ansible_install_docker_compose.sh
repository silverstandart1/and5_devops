#!/bin/sh
# ---------------------------------------------------------------------------------------
# Test Microservices Image with Docker / 2022_04_22 / ANa
# ---------------------------------------------------------------------------------------
echo ------------------------------------------------------ Create temp folder microservices
mkdir ./ansible
chmod 777 ./ansible
cd ansible

echo -e "\n"
echo ------------------------------------------------------ Create install_docker_compose.yml
cat << EOF > ./install_docker_compose.yml
# ----------------------------- install_docker_compose.yml START --------------------
---
- hosts: all
  become: true
  tasks:
    - name: install dependencies
      apt:
        name: "{{item}}"
        state: present
        update_cache: yes
      loop:
        - apt-transport-https
        - ca-certificates
        - curl
        - gnupg-agent
        - software-properties-common
    - name: add GPG key
      apt_key:
        url: https://download.docker.com/linux/debian/gpg
        state: present
    - name: add docker repository
      apt_repository:
        repo: deb https://download.docker.com/linux/debian bullseye stable
        state: present
    - name: install docker
      apt:
        name: "{{item}}"
        state: latest
        update_cache: yes
      loop:
        - docker-ce
        - docker-ce-cli
        - containerd.io
    - name: make sure docker is active
      service:
        name: docker
        state: started
        enabled: yes

    - name: Install docker-compose
      get_url: 
        url : https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64
        dest: /usr/local/bin/docker-compose
        mode: 0777

  handlers:
    - name: restart docker
      service: 
        name: docker 
        state: restarted

# ----------------------------- install_docker_compose.yml END --------------------
EOF
cat ./install_docker_compose.yml

echo -e "\n"
ansible-playbook /home/andrey/DEV_HOME/11/ansible/install_docker_compose.yml


