#!/bin/sh
# ---------------------------------------------------------------------------------------
# Script prints info about cluster / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

helm list --all-namespaces
echo "--------------------------------------------------------------------------------------------------------------------------------DISK and PV\r\n"
/home/infodba/yandex-cloud/bin/yc compute disk list
kubectl get pv

echo " \r\n"
echo "--------------------------------------------------------------------------------------------------------------------------------NODEs and PODs\r\n"
/home/infodba/yandex-cloud/bin/yc managed-kubernetes node-group list
/home/infodba/yandex-cloud/bin/yc compute instance list
kubectl get nodes -o wide

echo " \r\n"
kubectl get pods --all-namespaces

echo " \r\n"
kubectl get services -o wide --all-namespaces

echo " \r\n"
kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces

echo "--------------------------------------------------------------------------------------------------------------------------------Precisely for me\r\n"
kubectl --namespace and5-namespace get pods -l function=and5-app-server -o wide

echo " \r\n"
kubectl --namespace and5-namespace get pods -l function=and5-app-server -o YAML | grep podIP

echo " \r\n"
kubectl --namespace and5-namespace get pods -l function=and5-db-server -o wide

echo " \r\n"
kubectl --namespace and5-namespace get pods -l function=and5-db-server -o YAML | grep podIP

echo " \r\n"
