#!/bin/sh
# ---------------------------------------------------------------------------------------
# Helm installation and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

dir_name=/home/infodba/helm-v3.9.0-linux-amd64.tar.gz
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
elif [ -f "$dir_name" ]; then
    echo "Helm was already installed."
    exit
fi

wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz
tar -zxvf helm-v3.9.0-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
