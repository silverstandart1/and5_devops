#!/bin/sh
# ---------------------------------------------------------------------------------------
# Cleaning folder / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

    dir_name=$1
    if [ -d "$dir_name" ]; then
        echo "Removing $dir_name"
        rm -rf "$dir_name"
    elif [ -f "$dir_name" ]; then
        echo "File with this name already exists, not a directory."
        exit
    fi
    if mkdir "$dir_name"; then
        echo "Clean directory created: $dir_name"
        exit
    else
        echo "Creating directory failed: $dir_name"
        exit
    fi 
