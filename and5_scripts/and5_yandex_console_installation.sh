#!/bin/sh
# ---------------------------------------------------------------------------------------
# Yandex Console installation and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------
dir_name=/home/infodba/yandex-cloud
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
elif [ -f "$dir_name" ]; then
    echo "Yandex Console was already installed."
    exit
fi

curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
. /home/infodba/0_SSH/AND5_ACCESS_TOKEN.sh
/home/infodba/yandex-cloud/bin/yc config set token $AND5_ACCESS_TOKEN
