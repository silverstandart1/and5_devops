#!/bin/sh
# ---------------------------------------------------------------------------------------
# Docker installation and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

dir_name=/usr/bin/docker
if [ -d "$dir_name" ]; then
    echo "Docker was already installed."
    exit
fi

sudo apt-get install docker.io -y

if [ -d "/usr/local/bin/docker-compose" ]; then
    echo "Docker compose was already installed."
    exit
fi

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version



