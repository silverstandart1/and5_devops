#!/bin/sh
# ---------------------------------------------------------------------------------------
# Kubectl installation and5-svr / 2022_06_09 / ANa
# ---------------------------------------------------------------------------------------

dir_name=/home/infodba/kubectl
if [ -d "$dir_name" ]; then
    echo "Removing $dir_name"
    rm -rf "$dir_name"
elif [ -f "$dir_name" ]; then
    echo "Kubectl was already installed."
    exit
fi

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
